package com.task.currencyCalculator.currencyCalculator.service;

import com.task.currencyCalculator.currencyCalculator.domain.Country;
import com.task.currencyCalculator.currencyCalculator.domain.CurrencyRateFromNbp;
import com.task.currencyCalculator.currencyCalculator.domain.Rates;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyRateAPIServiceTest {

    private final static String url = "http://api.nbp.pl/api/exchangerates/rates/c/{currencyCode}/{currentDateExceptWeekends}/?format=json";
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private CurrencyRateAPIService currencyRateAPIService;

    @Test
    public void shouldGetAttributesFromNbpApi() {
        //given
        Rates rate = new Rates();
        rate.setAsk(4d);
        rate.setBid(3d);
        rate.setEffectiveDate("2018-05-04");
        List<Rates> rates = new ArrayList<>();
        rates.add(rate);
        CurrencyRateFromNbp currencyRateFromNbp = new CurrencyRateFromNbp();
        currencyRateFromNbp.setCode("USD");
        currencyRateFromNbp.setCurrency("dolar");
        currencyRateFromNbp.setRates(rates);
        Country country = new Country("Germany", "EUR", 800d, 20d);

        when(restTemplate.getForObject(eq(url), eq(CurrencyRateFromNbp.class),
                any(String.class), any(LocalDate.class))).thenReturn(currencyRateFromNbp);
        //when
        CurrencyRateFromNbp underTest = currencyRateAPIService.getDataFromApi(country);
        //then
        assertThat(underTest).isEqualTo(currencyRateFromNbp);
    }

    @Test
    public void shouldGetAttributesFromOtherDayIfDataNotFound() {
        //given
        Rates rate = new Rates();
        rate.setAsk(4d);
        rate.setBid(3d);
        rate.setEffectiveDate("2018-05-04");
        List<Rates> rates = new ArrayList<>();
        rates.add(rate);
        CurrencyRateFromNbp currencyRateFromNbp = new CurrencyRateFromNbp();
        currencyRateFromNbp.setCode("USD");
        currencyRateFromNbp.setCurrency("dolar");
        currencyRateFromNbp.setRates(rates);
        Country country = new Country("Germany", "EUR", 800d, 20d);

        when(restTemplate.getForObject(eq(url), eq(CurrencyRateFromNbp.class),
                any(String.class), any(LocalDate.class))).thenThrow(NullPointerException.class).thenReturn(currencyRateFromNbp);
        //when
        CurrencyRateFromNbp underTest = currencyRateAPIService.getDataFromApi(country);
        //then
        assertThat(underTest).isEqualTo(currencyRateFromNbp);
    }

    @Test
    public void shouldChangeDatesIfWeekend() {
        LocalDate sunday = currencyRateAPIService.getCurrentDateExceptWeekends(LocalDate.of(2018, 4, 22));
        LocalDate saturday = currencyRateAPIService.getCurrentDateExceptWeekends(LocalDate.of(2018, 4, 21));
        LocalDate friday = currencyRateAPIService.getCurrentDateExceptWeekends(LocalDate.of(2018, 4, 20));

        // testing if input dates are weekend days
        Assert.assertEquals(DayOfWeek.SUNDAY, LocalDate.of(2018, 4, 22).getDayOfWeek());
        Assert.assertEquals(DayOfWeek.SATURDAY, LocalDate.of(2018, 4, 21).getDayOfWeek());
        Assert.assertEquals(DayOfWeek.FRIDAY, LocalDate.of(2018, 4, 20).getDayOfWeek());

        // testing if weekend days were changed to friday before weekend
        Assert.assertEquals(DayOfWeek.FRIDAY, sunday.getDayOfWeek());
        Assert.assertEquals(DayOfWeek.FRIDAY, saturday.getDayOfWeek());
        Assert.assertEquals(DayOfWeek.FRIDAY, friday.getDayOfWeek());
        Assert.assertEquals(LocalDate.of(2018, 4, 20), sunday);
        Assert.assertEquals(LocalDate.of(2018, 4, 20), saturday);
        Assert.assertEquals(LocalDate.of(2018, 4, 20), friday);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenCurrencyCodeIsNotCorrect() {
        Country country = new Country("Germany", "EPR", 800d, 20d);
        currencyRateAPIService.income(country, 500);
    }

    @Test
    public void shouldCalculateIncomeWhenCountryAndWageIsGiven() {
        //given
        Rates rate = new Rates();
        rate.setAsk(4d);
        rate.setBid(3d);
        rate.setEffectiveDate("2018-05-04");
        List<Rates> rates = new ArrayList<>();
        rates.add(rate);
        CurrencyRateFromNbp currencyRateFromNbp = new CurrencyRateFromNbp();
        currencyRateFromNbp.setCode("USD");
        currencyRateFromNbp.setCurrency("dolar");
        currencyRateFromNbp.setRates(rates);
        Country country = new Country("Germany", "EUR", 800d, 20d);

        when(restTemplate.getForObject(eq(url), eq(CurrencyRateFromNbp.class),
                any(String.class), any(LocalDate.class))).thenReturn(currencyRateFromNbp);
        //when
        Double income = currencyRateAPIService.income(country, 15);
        //then
        assertThat(income).isEqualTo(3936d);
    }

    @Test
    public void shouldCalculateIncomeWhenCountryIsPoland() {
        //given
        Rates rate = new Rates();
        rate.setAsk(4d);
        rate.setBid(3d);
        rate.setEffectiveDate("2018-05-04");
        List<Rates> rates = new ArrayList<>();
        rates.add(rate);
        CurrencyRateFromNbp currencyRateFromNbp = new CurrencyRateFromNbp();
        currencyRateFromNbp.setCode("PLN");
        currencyRateFromNbp.setCurrency("zloty");
        currencyRateFromNbp.setRates(rates);
        Country country = new Country("Poland", "PLN", 800d, 20d);

        when(restTemplate.getForObject(eq(url), eq(CurrencyRateFromNbp.class),
                any(String.class), any(LocalDate.class))).thenReturn(currencyRateFromNbp);
        //when
        Double income = currencyRateAPIService.income(country, 15);
        //then
        assertThat(income).isEqualTo(1312d);
    }
}
