package com.task.currencyCalculator.currencyCalculator.service;

import com.task.currencyCalculator.currencyCalculator.domain.Country;
import com.task.currencyCalculator.currencyCalculator.domain.CurrencyRateFromNbp;
import com.task.currencyCalculator.currencyCalculator.domain.Rates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

@Service
public class CurrencyRateAPIService {

    private final static String url = "http://api.nbp.pl/api/exchangerates/rates/c/{currencyCode}/{currentDateExceptWeekends}/?format=json";
    private RestTemplate restTemplate;

    @Autowired
    public CurrencyRateAPIService() {
        this.restTemplate = new RestTemplate();
    }

    public LocalDate getCurrentDateExceptWeekends(LocalDate date) {
        if (date.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
            return date.minusDays(1L);
        } else if (date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            return date.minusDays(2L);
        } else {
            return date;
        }
    }

    public CurrencyRateFromNbp getDataFromApi(Country country) {
        CurrencyRateFromNbp currencyRateFromNbp = new CurrencyRateFromNbp();
        String currencyCode = country.getCurrencyCode();
        LocalDate currentDateExceptWeekends = getCurrentDateExceptWeekends(LocalDate.now());
        boolean success = false;

        //check if currency code added in Country is ISO format
        if (!Currency.getAvailableCurrencies().toString().contains(currencyCode)) {
            throw new IllegalArgumentException("Currency code is not in ISO format");
        }
        //check if country is Poland
        if (currencyCode.equals("PLN")) {
            currencyRateFromNbp.setCode("PLN");
            currencyRateFromNbp.setCurrency("polish zloty");
            Rates rates = new Rates();
            rates.setBid(1d);
            rates.setAsk(1d);
            List<Rates> ratesList = new ArrayList<>();
            ratesList.add(rates);
            rates.setEffectiveDate(LocalDate.now().toString());
            currencyRateFromNbp.setRates(ratesList);
            return currencyRateFromNbp;
        }

        try {
            currencyRateFromNbp = restTemplate.getForObject(url, CurrencyRateFromNbp.class,
                    currencyCode, currentDateExceptWeekends);
            success = true;
        } catch (HttpStatusCodeException | NullPointerException ex) {
            System.out.println("Data not found, checking currency rate from another date");
        }

        //if there is no current data at sever looking for data from one day before
        if (!success) {
            currentDateExceptWeekends = getCurrentDateExceptWeekends(LocalDate.now().minusDays(1));
            currencyRateFromNbp = restTemplate.getForObject(url, CurrencyRateFromNbp.class,
                    currencyCode, currentDateExceptWeekends);
        }

        return currencyRateFromNbp;
    }

    public Double income(Country country, int hourRate) {
        Double result;
        final int workingDays = 22;
        final int workingHours = 8;
        CurrencyRateFromNbp dataFromApi = getDataFromApi(country);

        result = (((hourRate * workingDays * workingHours)
                * (1 - (country.getTaxPercentage() / 100))) - country.getFixedCost())
                * dataFromApi.getRates().get(0).getBid();
        return result;
    }
}
