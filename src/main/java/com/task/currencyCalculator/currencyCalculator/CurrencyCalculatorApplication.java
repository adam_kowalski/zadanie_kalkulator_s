package com.task.currencyCalculator.currencyCalculator;

import com.task.currencyCalculator.currencyCalculator.domain.Country;
import com.task.currencyCalculator.currencyCalculator.domain.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyCalculatorApplication implements CommandLineRunner{

    @Autowired
    CountryRepository countryRepository;

    public static void main(String[] args) throws Exception{
        SpringApplication.run(CurrencyCalculatorApplication.class, args);
    }

    //override run to initialize database with countries
    @Override
    public void run(String... args) throws Exception {
        countryRepository.save(new Country("Poland", "PLN", 1200d, 19d));
        countryRepository.save(new Country("Germany", "EUR", 800d, 20d));
        countryRepository.save(new Country("United Kingdom", "GBP", 600d, 25d));
    }
}
