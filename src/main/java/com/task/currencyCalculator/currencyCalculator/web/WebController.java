package com.task.currencyCalculator.currencyCalculator.web;

import com.task.currencyCalculator.currencyCalculator.domain.Country;
import com.task.currencyCalculator.currencyCalculator.domain.CountryRepository;
import com.task.currencyCalculator.currencyCalculator.domain.Rates;
import com.task.currencyCalculator.currencyCalculator.service.CurrencyRateAPIService;
import com.task.currencyCalculator.currencyCalculator.domain.submitForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

@Controller
public class WebController implements WebMvcConfigurer {

    private CurrencyRateAPIService currencyRateAPIService;

    @Autowired
    CountryRepository countryRepository;
    @Autowired
    public WebController(CurrencyRateAPIService currencyRateAPIService) {
        this.currencyRateAPIService = currencyRateAPIService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String countriesList(Model model) {
        submitForm form = new submitForm();
        model.addAttribute("submitForm", form);
        model.addAttribute("countries", countryRepository.findAll());
        return "index";
    }

    @RequestMapping(value = "/calculateIncome", method = RequestMethod.POST)
    public String calculateIncome(@Valid @ModelAttribute("submitForm") submitForm submitForm,
                                  BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "error";
        }

        Country country = countryRepository.findById(submitForm.getCountryId()).get();
        Rates rates = currencyRateAPIService.getDataFromApi(country).getRates().get(0);

        Double income = currencyRateAPIService.income(country, submitForm.getHourRate());

        model.addAttribute("ratesFromApi", rates);
        model.addAttribute("country", country);
        model.addAttribute("income", String.format("%.2f",income));
        return "result";
    }


}
