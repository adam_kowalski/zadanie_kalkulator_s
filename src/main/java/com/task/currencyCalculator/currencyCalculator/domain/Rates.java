package com.task.currencyCalculator.currencyCalculator.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rates {
    private Double bid;
    private Double ask;
    private String effectiveDate;
}
