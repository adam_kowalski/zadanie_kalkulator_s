package com.task.currencyCalculator.currencyCalculator.domain;

import lombok.Data;

@Data
public class submitForm {

    private int hourRate;
    private Long countryId;
}
