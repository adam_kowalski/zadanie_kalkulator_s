package com.task.currencyCalculator.currencyCalculator.domain;

import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Long> {
}
