package com.task.currencyCalculator.currencyCalculator.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String currencyCode;
    private Double fixedCost;
    private Double taxPercentage;

    public Country(String name, String currencyCode, Double fixedCost, Double taxPercentage) {
        this.name = name;
        this.currencyCode = currencyCode;
        this.fixedCost = fixedCost;
        this.taxPercentage = taxPercentage;
    }


}