package com.task.currencyCalculator.currencyCalculator.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyRateFromNbp {
    private String code;
    private String currency;
    private List<Rates> rates;
}
