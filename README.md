# Zadanie_kalkulator_s

App calculating monthly net income in PLN on foreign contracts.

### Installing

Clone repository: git clone
https://bitbucket.org/adam_kowalski/zadanie_kalkulator_s.git


If you are using Gradle, you can run the application using ./gradlew bootRun. Or you can build the JAR file using ./gradlew build. Then you can run the JAR file:

```
java -jar build/libs/currencyCalculator-0.0.1-SNAPSHOT.jar
```

If you are using Maven, you can run the application using ./mvnw spring-boot:run. Or you can build the JAR file with ./mvnw clean package. Then you can run the JAR file:

```
java -jar target/currencyCalculator-0.0.1-SNAPSHOT.jar
```
Run the application from CurrencyCalculatorApplication and get to http://localhost:8080/ in your browser
```
2018-04-25 23:04:28.337  INFO 17200 --- [main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2018-04-25 23:04:28.337  INFO 17200 --- [main] c.t.c.c.CurrencyCalculatorApplication    : Started CurrencyCalculatorApplication in 3.657 seconds (JVM running for 4.374)
```

## Running

After building and running the application, get to http://localhost:8080/ in your browser.
On the main page available countries are displayed. Form requires to put hourly wage and choosing country from list.
After filling the form user is redirected to page with the result.

## Testing

Test are divided into two categories:
* service tests
* web controller tests

### What is tested (by now)

* Tests include checking if data received from NBP Api are correct.
* Checks if LocalTime.now is no longer taking saturdays and sundays due to no data on API servers during the weekend.
* Throwing exception while adding new Country object with countryCode different than available in ISO format.
* Mapping in WebController
* Calculating income for Poland and other countries with mocked data

## Developement

* Countries are kept in database to make it easier to add new countries in the future.
* Contry.class stores information about country name, country code in ISO format, fixed cost and taxes.
* During the weekend current data is being changed to friday before weekend - NBP Api does not share currency ratio during weekends.
* If data is not available in NBP Api for current day, data are being downloaded from one day earlier 
* In CurrencyCalculatorApplication.class method run is overrided to initialize starting database with Poland, Germany and Great Britain.
* HTML files are prepared to generate as many cards and inputs as there is countries.


## Built With

* [Spring](https://spring.io/) - Spring: the source for modern java
* [Maven](https://maven.apache.org/) - Dependency Management
* [Thymeleaf](http://www.thymeleaf.org) - Modern server-side Java template engine
* [Bootstrap](https://startbootstrap.com/) - Start Bootstrap is a collection of free to download Bootstrap themes and templates